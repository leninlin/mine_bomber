class @UnitController
	constructor: (params) ->
		@socket = params.socket
		@player = new Unit(
			object_id: "unit"
			keys_type: "normal"
			socket: @socket
			character: {}
		)
		@players = []

		self = this

		@socket.on 'move', (data) ->
			unit = self.players[data.id]
			pos = unit.getAbsolutePosition data.position.ceils
			unit.changeDirection data.position.direction

			if unit.events.changeDirection
				unit.character.setAnimation data.position.direction
				unit.events.changeDirection = false

			unit.actions data.position.action, false

			unit.old_position.x = unit.position.x
			unit.old_position.y = unit.position.y
			unit.position.x = pos.x
			unit.position.y = pos.y

			unit.moveTo(pos.x, pos.y)

		@socket.on 'joined', (data) ->
			self.addUser data

		@socket.on 'left', (data) ->
			self.players[data.id].leaveGame()

			delete self.players[data.id]

		@socket.on 'drilling', (data)  ->
			$.each data.ceils, ->
				map.changeCell @position.x, @position.y, @tail.sprite, false

		@socket.on 'takeGold', (data)  ->
			$.each data.ceils, ->
				map.changeCell @position.x, @position.y, @tail.sprite, false

		@socket.on 'boom', (data) ->
			map.changeCells data.ceils

		@socket.on 'updateStat', (data) ->
			Stats.gold data.index, data.gold
			Stats.damage data.index, data.hp
			Stats.bomb data.index, data.tool

		@socket.on 'died', (data) ->
			Stats.died data.index
			self.player.leaveGame()

		@socket.on 'set_bomb', (data) ->
			map.animateSprite data.ceil.pos.x*self.player.grid_size
			, data.ceil.pos.y*self.player.grid_size
			, data.ceil.sprite
			, data.ceil.spriteFrames
			, data.ceil.timer
			, data.ceil.animname

	addCharacter: (object) ->
		@player.character = object

	joinRoom: (room_id) ->
		self = this
		@socket.emit 'event',
			event: "join_room"
			room: room_id
		, (data) ->
			Stats.showPanel data.index

			self.player.id
			self.player.position = 
				x: data.position.ceils[0].x * self.player.grid_size
				y: data.position.ceils[0].y * self.player.grid_size

			self.player.sprite = data.sprite
			self.player.createCharacter()

			self.socket.emit 'event',
				event: "get_room_users"
			, (data) ->
				if not $.isEmptyObject data
					$.each data , ->
						self.addUser this


	addUser: (user_data) ->
		@players[user_data.id] = new Unit(
			id: user_data.id
			object_id: 'unit' + user_data.id
			socket: self.socket
			start_position: user_data.position
			index: user_data.index
			sprite: user_data.sprite
			toolId: user_data.toolId
		)

		@players[user_data.id].createCharacter()
		Stats.showPanel user_data.index

	processUsersPositions = (players) ->
		console.log 'process'

