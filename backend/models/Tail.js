var extend = require('extend'),
	math = require('math');

// Tail model
exports.Tail = function (type, pos) {

	var tailType = exports.Types[type];

	var defParams = {
		pos: pos,
		type: type,
		spriteFames: 0,
		sprite: ['sprites/dust.png'],
		solidity: 0,
		bottom: false,
		next: [0],

		damage: function (power) {
			if (this.solidity) {
				if (power < this.solidity) {
					this.solidity = this.solidity - power;
				} else {
					power = power - this.solidity;
					if (this.next.length) {
						this.selectNext();
					}
					var tail = new exports.Tail(this.next);
					tail = tail.damage(power);

					return tail;
				}
			}

			return this;
		},

		selectSprite: function () {
			this.sprite = this.sprite[math.floor(math.random() * this.sprite.length)];
		},

		selectNext: function () {
			this.next = this.next[math.floor(math.random() * this.next.length)];
		},
	};
	for (n in tailType) {
		defParams[n] = tailType[n];
	}
	tailType = defParams;

	tailType.selectSprite();
	tailType.selectNext();

	return tailType;
};

exports.Types = {
	0: {
		name: 'ground',
		bottom: true,
		sprite: ['sprites/dust.png'],
		solidity: 0,
	},

	1: {
		name: 'sand',
		sprite: ['sprites/block_modern/dust_block.png'],
		solidity: 2,
		next: [3, 4],
	},

	2: {
		name: 'stone',
		sprite: ['sprites/sprites png/stone_block/1.png'],
		solidity: 8,
		next: [9, 10],
	},

	3: {
		name: 'breaksand',
		sprite: ['sprites/sprites png/dust_block/2.png'],
		solidity: 2,
		next: [4, 4, 4, 8],
	},

	4: {
		name: 'verybreaksand',
		sprite: ['sprites/sprites png/dust_block/3.png'],
		solidity: 1,
		next: [8, 0],
	},

	5: {
		name: 'fire',
		bottom: true,
		sprite: ['sprites/sprites png/boom.png'],
		solidity: 0,
		spriteFrames: 18,
		timer: 500,
	},

	6: {
		name: 'wall',
		solidity: 0,
		sprite: ['sprites/border.png'],
	},

	7: {
		name: 'bomb',
		solidity: 0,
		sprite: ['sprites/sprites png/bomb.png'],
		spriteFrames: 3,
		timer: 4000,
	},

	8: {
		name: 'scrapsand',
		sprite: ['sprites/sprites png/dust_block/4.png'],
		solidity: 1,
		next: [0],
	},

	9: {
		name: 'breakstone',
		sprite: ['sprites/sprites png/stone_block/2.png'],
		solidity: 6,
		next: [10, 11],
	},

	10: {
		name: 'verybreakstone',
		sprite: ['sprites/sprites png/stone_block/3.png'],
		solidity: 4,
		next: [11, 0],
	},

	11: {
		name: 'scrapstone',
		sprite: ['sprites/sprites png/stone_block/4.png'],
		solidity: 2,
		next: [0],
	},

    12: {
        name: 'gold',
        bottom: true,
        solidity: 0,
        gold: 1,
        sprite: ['sprites/sprites png/gold.png']
    },

    13: {
        name: 'gold2',
        bottom: true,
        solidity: 0,
        gold: 2,
        sprite: ['sprites/sprites png/gold.png']
    },

    14: {
        name: 'gold3',
        bottom: true,
        solidity: 0,
        gold: 4,
        sprite: ['sprites/sprites png/gold.png']
    },

    15: {
        name: 'corsair',
        solidity: 0,
        sprite: ['sprites/sprites png/corsair.png'],
        spriteFrames: 18,
		timer: 1000,
    },

    16: {
        name: 'TNT',
        solidity: 0,
        sprite: ['sprites/sprites png/tnt.png'],
        spriteFrames: 0.5,
		timer: 6000,
    }

};