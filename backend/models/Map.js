// Map generator model
exports.Map = function () {

/*    sizeRow = 48;
    sizeColumn = 64;
*/
    sizeRow = 11;
    sizeColumn = 29;

    const countRow = Math.ceil(sizeRow / 16) * 16;
    const countColumn = Math.ceil(sizeColumn / 16) * 16;

    map = Array(countRow);

    for (var row = 0; row < countRow; row++) {
        map[row] = Array(countColumn);
    }

    // fill sand
    fill_block(0, countRow - 1, 0, countColumn, 1);

    gen_map();
    smooth_map();
    normalize_map();

    function gen_map()
    {
        for (var row = 0; row < countRow; row += 16) {
            for (var column = 0; column < countColumn; column += 16) {
                var count_block = get_random_count_block();

                fill_blocks(row, column, count_block);
            }
        }
    }

    function smooth_map()
    {
        for (var row = 0; row < countRow; row += 4) {
            for(var column = 0; column < countColumn; column += 4) {
                gen_smooth_map(row, row + 3, column, column + 3);
            }
        }
    }

    function normalize_map()
    {
        var map_slice = Array(sizeRow);

        for (var row = 0; row < sizeRow; row++) {
            map_slice[row] = map[row].slice(0, sizeColumn);
        }

        // wall
        for (var column = 0; column < sizeColumn; column++) {
            map_slice[0][column] = 6;
            map_slice[sizeRow - 1][column] = 6;
        }

        for (var row = 0; row < sizeRow; row++) {
            map_slice[row][0] = 6;
            map_slice[row][sizeColumn - 1] = 6;
        }

        // init player place
        for (var column = 1; column <= 3; column++) {
            map_slice[1][column] = 0;
            map_slice[1][sizeColumn - column - 1] = 0;

            map_slice[sizeRow - 2][column] = 0;
            map_slice[sizeRow - 2][sizeColumn - column - 1] = 0;
        }

        for (var row = 1; row <= 3; row++) {
            map_slice[row][1] = 0;
            map_slice[sizeRow - row - 1][1] = 0;

            map_slice[row][sizeColumn - 2] = 0;
            map_slice[sizeRow - row - 1][sizeColumn - 2] = 0;
        }

        var countGold = Math.round(Math.random() * 4) + 2;

        for (var gold = 1; gold <= countGold; gold++) {

            row = get_random_int(sizeRow - 4) + 2;
            column = get_random_int(sizeColumn - 4) + 2;

            map_slice[row][column] = 13;
        }

        map = map_slice;
    }

    function get_random_count_block()
    {
        var rand_block = Math.random();
        var block = 0;

        if (rand_block < 0.3) {
            block = 3;
        } else if (rand_block < 0.7) {
            block = 4;
        } else if (rand_block < 0.9) {
            block = 2;
        } else {
            block = 1;
        }

        return block;
    }

    function fill_blocks(row_begin, column_begin, count_block)
    {
        blocks_map = get_random_blocks(count_block);

        var row = row_begin;
        var column = column_begin;

        for (var row_block = 0; row_block <= 3; row_block++) {
            for (var column_block = 0; column_block <= 3; column_block++) {
                if (blocks_map[row_block][column_block]) {
                    var pos_block = row_begin + row_block * 4;
                    var pos_column = column_begin + column_block * 4;

                    fill_block(pos_block, pos_block + 3, pos_column, pos_column + 3, 2);
                }
            }
        }
    }

    function get_random_blocks(count_blocks)
    {
        var blocks = Array(count_blocks - 1);

        var blocks_map = Array(4);

        for (var row = 0; row < 4; row++) {
            blocks_map[row] = Array(4);
            for (var column = 0; column < 4; column++) {
                blocks_map[row][column] = false;
            }
        }

        var block_row = Math.round(Math.random() * 3);
        var block_column = Math.round(Math.random() * 3);
        var start_block_id = 0;

        blocks[0] = Array(block_row, block_column);

        blocks_map[blocks[0][0]][blocks[0][1]] = true;

        for (var index_block = 1; index_block < count_blocks; index_block++) {
            var next_block = get_next_block(blocks_map, blocks, start_block_id);

            blocks[index_block] = Array(next_block[0], next_block[1]);
            blocks_map[next_block[0]][next_block[1]] = true;

            start_block_id = index_block;
        }

        return blocks_map;
    }

    function get_next_block(blocks_map, blocks, block_id)
    {
        var block_row = blocks[block_id][0];
        var block_column = blocks[block_id][1];

        var step_row = get_random_int(2) - 1;
        var step_column = get_random_int(2) - 1;

        if (step_row == 0 && step_column == 0) {
            step_row = 1;
        }

        if (blocks_map[block_row][block_column]) {

            var step = 1;

            while (step <= 8) {

                if (check_zone(block_row, step_row) && check_zone(block_column, step_column)) {
                    if (!blocks_map[block_row + step_row][block_column + step_column]) {

                        return Array(block_row + step_row, block_column + step_column);
                    }
                }

                if (step_row == -1 && step_column < 1) {
                    step_column++;

                } else if (step_row < 1 && step_column == 1) {
                    step_row++;

                } else if (step_row == 1 && step_column > -1) {
                    step_column--;

                } else {
                    step_row--;
                }

                step++;
            }

        }

        console.log('error: not found next route');
        return false;
    }

    function check_zone(value, step)
    {
        if (value + step >= 0 && value + step < 4) {
            return true;
        }
    }

    function get_random_int(max)
    {
        return Math.round(Math.random() * max);
    }

    function fill_block(row_begin, row_end, column_begin, column_end, code_block)
    {
        for (var row = row_begin; row <= row_end; row++) {
            for(var column = column_begin; column <= column_end; column++)
                map[row][column] = code_block;
        }
    }

    function gen_smooth_map(row_begin, row_end, column_begin, column_end)
    {
        var row_div = Math.floor((row_begin + row_end) / 2);
        var column_div = Math.floor((column_begin + column_end) / 2);

        fill_smooth_map(row_begin, row_div, column_begin, column_div,  get_rand_block(0.5), 1);
        fill_smooth_map(row_begin, row_div, column_div + 1, column_end,  get_rand_block(0.5), 2);
        fill_smooth_map(row_div + 1, row_end, column_begin, column_div, get_rand_block(0.5), 3);
        fill_smooth_map(row_div + 1, row_end, column_div + 1, column_end, get_rand_block(0.5), 4);
    }

    function fill_smooth_map(row_begin, row_end, column_begin, column_end, code_block, pos_block)
    {
        adjacent = false;

        if (row_begin - 1 > 0 && (pos_block == 1 || pos_block == 2)) {
            adjacent = map[row_begin - 1][column_begin] == code_block;
        }

        if (row_end + 2 < countRow && (pos_block == 3 || pos_block == 4)) {
            adjacent = map[row_end + 2][column_begin] == code_block;
        }

        if (column_begin - 1 > 0 && (pos_block == 1 || pos_block == 3)) {
            adjacent = map[row_begin][column_begin - 1] == code_block;
        }

        if (column_end + 2 < countColumn && (pos_block == 2 || pos_block == 4)) {
            adjacent = map[row_begin][column_begin + 2] == code_block;
        }

        if (adjacent) {
            fill_block(row_begin, row_end, column_begin, column_end, code_block)
        }
    }

    function get_rand_block(chance)
    {
        var rand_block = Math.random();

        if (rand_block <= chance) {
            return 0;
        } else {
            return 2;
        }
    }

    return {
        map: map,
        sizeRow: sizeRow,
        sizeColumn: sizeColumn,
    }
}
