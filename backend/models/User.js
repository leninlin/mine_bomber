// User model
exports.User = function (socketId) {

	var defaultParams = {
		1: {
			sprite: 'sprites/bomber/red_player.png',
			position: {
				ceils: [
					{x: 1, y: 1, fill: 1}
				],
			},
		},

		2: {
			sprite: 'sprites/bomber/blue_player.png',
			position: {
				ceils: [
					{x: 27, y: 9, fill: 1}
				],
			},
		},

		3: {
			sprite: 'sprites/bomber/green_player.png',
			position: {
				ceils: [
					{x: 27, y: 1, fill: 1}
				],
			},
		},

		4: {
			sprite: 'sprites/bomber/orange_player.png',
			position: {
				ceils: [
					{x: 1, y: 9, fill: 1}
				],
			},
		},
	};

	return {
		id: socketId,
		hp: 10,
        gold: 0,
		index: 1,
		room: null,
		socket: io.sockets.sockets[socketId],
		position: {},
		tools: {},
		currentTool: 1,

		joinRoom: function (room) {
			if (this.room) {
				this.leaveRoom(rooms[this.room]);
			}

			this.room = room.id;
			room.addUser(this.id);
			this.socket.join(room.id);

			this.setDefaultParams();

			this.socket.broadcast.to(room.id).emit('joined', this.getInfo());
		},

		leaveRoom: function (room) {
			room.removeUser(this.id);
			this.socket.leave(room.id);
			this.room = null;

			this.socket.broadcast.to(room.id).emit('left', this.getInfo());
		},

		getInfo: function () {
			var info = {
				id: this.id,
				room: this.room,
				position: this.position,
				index: this.index,
				sprite: this.sprite,
				hp: this.hp,
				toolId: this.currentTool,
                gold: this.gold,
                tool: this.tools[this.currentTool],
			};
			return info;
		},

		setDefaultParams: function () {
			var index = 1;
			var indexes = {1: false, 2: false, 3: false, 4: false};
			for (userId in rooms[this.room].users) {
				if (userId !== this.id) {
					indexes[users[userId].index] = true;
				}
			}
			for (i in indexes) {
				if (!indexes[i]) {
					index = i;
					break;
				}
			}
			this.index = index;
			this.position = defaultParams[index].position;
			this.sprite = defaultParams[index].sprite;
		},

		checkPosition: function (ceil, map) {
			var tail = map[ceil.y][ceil.x];
			var access = true;
			if (!tail.bottom) {
				access = false;
			}

			return access;
		},

		damage: function (power) {
			this.hp -= power;

			if (this.hp <= 0) {
				io.sockets.in(this.room).emit('died', this.getInfo());

				//this.leaveRoom(rooms[this.room]);
			} else {
				io.sockets.in(this.room).emit('updateStat', this.getInfo());
				
			}

		},

		changeTool: function () {
			if (typeof this.tools[this.currentTool+1] !== 'undefined') {
				this.currentTool = this.currentTool+1;
			} else {
				this.currentTool = 1;
			}

			io.sockets.in(this.room).emit('updateStat', this.getInfo());

			return this.currentTool;
		}

	}
}